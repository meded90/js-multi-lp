class GeoLp
  default: {

  }
  resultGeo: null
  isCyrillic: false
  resultGeoCyrillic: {}
  constructor: ()->

  load: (cb, index=false)->
    unless index
      index = setTimeout(=>
        cb({})
      ,5000)
    if @resultGeo
      unless @isCyrillic
        cb(@resultGeo)
        clearTimeout(index)
      else if (
        @resultGeoCyrillic['country_name'] and @resultGeoCyrillic['region_name'] and @resultGeoCyrillic['region_name'])
        cb(@resultGeo)
        clearTimeout(index)
      else
        setTimeout =>
          @load(cb,index)
        , 50
    else
      setTimeout =>
        @load(cb,index)
      , 50

  insertEl: (el)->
    unless el then return
    paremetrs = @getParameter(el)
    @insertValue(el, paremetrs)

  insertThis: ()->
    id = this.getRandomId()
    document.write("<span id='#{id}' class='multiLabel' style='display: none !important;'></span>")
    this.label = document.getElementById(id)
    this.parent = label.parentNode
    this.parent.removeChild(this.label)
    paremetrs = @getParameter(this.parent)
    @insertValue(this.parent, paremetrs)

  findText: ()->
    texts = document.getElementsByTagName('span')
    for el,index in texts
      paremetrs = @getParameter(el)
      @insertValue(el, paremetrs)

  getParameter: (element)->
    param = this.getAttr element, 'data-geolp' || ''
    name = 'city'
    if /country/im.test param
      name = 'country_name'
    else if /region/im.test param
      name = 'region_name'
    else if /city/im.test param
      name = 'city'
    {
      name: name
      case:  parseInt(param?.replace(/\D+/g,"")) || 1
    }

  insertValue: (el, parameter)->
    @load (data)=>
      newVal = @resultGeo?[parameter.name] || ''
      if @isCyrillic
        newVal = @resultGeoCyrillic[parameter.name][parameter.case] ||@resultGeoCyrillic[parameter.name][1] || newVal
      newVal = decodeURIComponent newVal
      @insertText el, newVal if  newVal
      @showEl el


  insertText: (el, text) ->
    if (!el) then return

    if (document.all)
      if text
        el.innerText = text
    else
      if text
        el.textContent = text

  showEl: (el) ->
    el?.style?.opacity = 1


  #=======================================
  #        Поиск ид
  #=======================================
  ranomIdLitst = []
  getRandomArbitary: (min = 0, max = 100) ->
    Math.floor Math.random() * (max - min) + min

  ranomIdLitst: {}
  getRandomId: ->
    newId = this.getRandomArbitary(0, 999999)
    unless GeoLp::ranomIdLitst[newId]
      unless document.getElementById(newId)
        GeoLp::ranomIdLitst[newId] = true
        return newId
      else
        return this.getRandomId()
    else
      return this.getRandomId()


  getAttr: (ele, attr)->
    result = (ele.getAttribute && ele.getAttribute(attr)) || null
    if( !result )
      attrs = ele.attributes
      length = attrs.length
      for i in [0..length]
        if(attrs[i] and attrs[i].nodeName == attr)
          result = attrs[i].nodeValue
    result

  addStyle: ->
    style  = document.createElement('style')
    style.type = 'text/css'
    @insertText style, '[data-geolp]{opacity:0}'
    document.getElementsByTagName('head')[0].appendChild style

  loadJSONP: do ->
    unique = 0
    (url, callback, context) ->
      # INIT
      name = '_jsonp_' + unique++
      if url.match(/\?/)
        url += '&callback=' + name
      else
        url += '?callback=' + name
      # Create script
      script = document.createElement('script')
      script.type = 'text/javascript'
      script.src = url
      # Setup handler
      window[name] = (data) ->
        callback.call context or window, data
        document.getElementsByTagName('head')[0].removeChild script
        script = null
        delete window[name]
        return

      # Load JSON
      document.getElementsByTagName('head')[0].appendChild script
      return


GeoLp::loadJSONP '//freegeoip.net/json/',
  (data)->
    GeoLp::resultGeo = data
#    regExpCyrillic = /[\u0400-\u04FF]/gi
#    if regExpCyrillic.test data.country_name
#      GeoLp::isCyrillic = true
#      GeoLp::isCyrillicCountry = true
#      GeoLp::loadJSONP "//export.yandex.ru/inflect.xml?key=pdct.1.1.20150303T062110Z.fe3618adc0ab34f8.0b36f9975478d8eaf63c0fd6cd239e67a65b5daf&name=#{encodeURI(data.country_name)}&format=json",
#        (data)->
#          GeoLp::resultGeoCyrillic['country_name'] = data
#
#    if regExpCyrillic.test data.region_name
#      GeoLp::isCyrillic = true
#      GeoLp::isCyrillicRegion = true
#      GeoLp::loadJSONP "//export.yandex.ru/inflect.xml?name=#{encodeURI(data.region_name)}&format=json",
#        (data)->
#          GeoLp::resultGeoCyrillic['region_name'] = data
#
#    if regExpCyrillic.test data.city
#      GeoLp::isCyrillic = true
#      GeoLp::isCyrillicCity = true
#      GeoLp::loadJSONP "//export.yandex.ru/inflect.xml?name=#{encodeURI(data.city)}&format=json",
#        (data)->
#          GeoLp::resultGeoCyrillic['city'] = data


GeoLp::addStyle()




window.initGeoEl = (el)->
  new GeoLp().insertEl(el)

window.initGeoText = ()->
  new GeoLp().insertThis()

window.initGeoAll = ()->
  new GeoLp().findText()

index = setInterval(->
#  initMultiImg()
  if "loaded" == document.readyState or document.readyState == "complete"
    clearInterval index
    initGeoAll()
, 10)