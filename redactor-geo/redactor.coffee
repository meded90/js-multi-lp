define ['jquery',
        '../modal-geo/modal',
        '../gepLp',
        'bower_components/redactorjs/redactor',
        'bower_components/font-awesome/css/font-awesome.css!'
        './redactor.css!'
  ],
  ( $, ModalGeoLp ) ->
    window.RedactorPlugins = {}  unless window.RedactorPlugins
    window.initGeoText = ( name )->
    window.initGeoAll = ( name )->
    RedactorPlugins.geolp = ->
      nameMark: 'utm_term'
      init: ->
        window.initGeoText = ( name )->
        window.initGeoAll = ( name )->

        button = @button.add( "geolp", "Мульти лендинг" )
        @button.setAwesome( 'geolp', 'fa-globe' )
        this.button.addCallback( button, this.geolp.show )
        this.geolp.initCheckActive()
        return

      show: ()->
        this.selection.save();
        new ModalGeoLp( {
          vars: this.geolp.select.attr('data-geolp') || '',
          callback: ( data ) =>
            this.geolp.insert( data )
          isRemove: !!this.geolp.select.size()
          callbackRemove: =>
            this.geolp.remove(this.geolp.select)

        } )

      initCheckActive: ->
        this.$editor.on('mouseup.redactor keyup.redactor focus.redactor', $.proxy(this.geolp.checkActive, this));

      checkActive: ()->
        current = this.selection.getCurrent();
        currentEl = $(current).closest('[data-geolp]')
        if currentEl.size()
          this.geolp.select = currentEl
          this.button.setActive('geolp');
        else
          this.geolp.select = $()
          this.button.setInactive('geolp');
      remove:(target)->
        target.removeAttr('data-geolp')
        target.find('[data-geolp]').removeAttr('data-geolp')

        this.selection.restore()
        this.code.sync()

      insert: ( data )->
        if this.geolp.select.size()
          $el = this.geolp.select
        else
          this.inline.removeFormat()
          this.inline.formatMultiple( 'span' );
          $el = $ this.selection.getCurrent()

        this.geolp.remove($el)

        $el.attr( 'data-geolp', data.vars || '' )

        this.selection.restore()
        this.code.sync()


