define ['jquery',
        '../modal/modal',
        'bower_components/redactorjs/redactor',
        'bower_components/font-awesome/css/font-awesome.css!'
        './redactor.css!'
  ],
  ( $, ModalMultiLp ) ->
    window.RedactorPlugins = {}  unless window.RedactorPlugins
    RedactorPlugins.multilp = ->
      nameMark: 'utm_term'
      init: ->
        window.multiText = ( name )->
        window.multiBg = ( name )->
        window.multiHide = ( name )->
        window.multiShow = ( name )->



        button = @button.add( "multilp", "Мульти лендинг" )
        @button.setAwesome( 'multilp', 'fa-magnet' )
        this.button.addCallback( button, this.multilp.show )
        this.multilp.initCheckActive()
        return
      show: ()->
        this.selection.save();
        new ModalMultiLp( {
          name: this.multilp.select.attr('data-multi-name') || this.nameMark,
          vars: this.multilp.select.attr('data-multi-value') || '',
          callback: ( data ) =>
            this.multilp.insert( data )
          isRemove: !!this.multilp.select.size()
          callbackRemove: =>
            this.multilp.remove(this.multilp.select)

        } )

      initCheckActive: ->
        this.$editor.on('mouseup.redactor keyup.redactor focus.redactor', $.proxy(this.multilp.checkActive, this));

      checkActive: ()->
        current = this.selection.getCurrent();
        currentEl = $(current).closest('[data-multi-name]')
        if currentEl.size()
          this.multilp.select = currentEl
          this.button.setActive('multilp');
        else
          this.multilp.select = $()
          this.button.setInactive('multilp');
      remove:(target)->
        target.removeAttr('data-multi-name').removeAttr('data-multi-value')
        target.find('[data-multi-name]').removeAttr('data-multi-name').removeAttr('data-multi-value')

        this.selection.restore()
        this.code.sync()

      insert: ( data )->
        if this.multilp.select.size()
          $el = this.multilp.select
        else
          this.inline.removeFormat()
          this.inline.formatMultiple( 'span' );
          $el = $ this.selection.getCurrent()

        this.multilp.remove($el)
        $el.attr( 'data-multi-name', data.name )
        if data.vars
          $el.attr( 'data-multi-value', data.vars )
        unless $el.find('.js-multi-lp')
          $el.append("<script class='js-multi-lp'> window.multiText('val1') </script>")

        this.selection.restore()
        this.code.sync()


