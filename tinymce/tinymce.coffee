define ['jquery',
        '../modal/modal',

        './tinymce.css!'
  ],
  ( $, ModalMultiLp ) ->
    window.multiText = ( name )->
    window.multiBg = ( name )->
    window.multiHide = ( name )->
    window.multiShow = ( name )->


    tinymce.PluginManager.add( 'multilp', ( editor ) ->
      each = tinymce.util.Tools.each
      $ = tinymce.dom.DomQuery;
      j = 0

      findParens = ()->
        parents = editor.dom.getParents( editor.selection.getNode() )
        parent = null
        if editor.selection.isCollapsed()
          parent = editor.selection.getNode()
        for item in parents
          if editor.dom.getAttribs( item )['data-multi-name']
            parent = item

        parent

      registrPlugin = ()->
        dom = editor.dom

        editor.formatter.register( 'multilp', {
          inline: 'span',
          links: true,
          remove_similar: true,
          remove: 'all',
          split: true,
          deep: true,
          collapsed: true

          onmatch: ()->
            return true

          onformat: ( elm, fmt, vars, node )->
            each( vars, ( value, key )->
              dom.setAttrib( elm, key, value )
            )
#            $
        } )

      onButtonClick = ( e ) ->
        do registrPlugin


        parent = findParens()

        name = 'var1'
        vars = ''
        if parent
          name = editor.dom.getAttrib( parent, 'data-multi-name', 'var1' )
          vars = editor.dom.getAttrib( parent, 'data-multi-val', null )

        new ModalMultiLp( {
          name: name,
          vars: vars,
          callback: ( data ) =>
            editor.focus()

            editor.formatter.apply( 'multilp', {
              'data-multi-name': data.name || ''
              'data-multi-val': data.vars || ''
            }, parent )


            el = editor.selection.getStart()
            $( '.js-multi-lp', el ).remove()
            $( el ).append( '<script class="js-multi-lp">window.multiText()</script>' )
            debugger

            editor.nodeChanged()


          isRemove: !!editor.dom.getAttribs( parent )['data-multi-name']
          callbackRemove: =>
            editor.formatter.remove( 'multilp', {
              'data-multi-name': name || ''
              'data-multi-val': vars || ''
            }, parent )
        } )


      editor.addButton 'multilp',
        icon: 'fa">
<svg
  style=" -webkit-filter: drop-shadow( 1px 1px 0px #fff );
            filter: drop-shadow( 1px 1px 0px #fff );"
  xmlns="http://www.w3.org/2000/svg"
  xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="16" height="16" viewBox="0 0 32 32">

<g id="icomoon-ignore"></g>
<path  d="M0 17.143v-2.286q0-0.464 0.339-0.804t0.804-0.339h6.857q0.464 0 0.804 0.339t0.339 0.804v2.286q0 0.929 0.42 1.607t0.955 1.018 1.268 0.536 1.143 0.232 0.786 0.036 0.786-0.036 1.143-0.232 1.268-0.536 0.955-1.018 0.42-1.607v-2.286q0-0.464 0.339-0.804t0.804-0.339h6.857q0.464 0 0.804 0.339t0.339 0.804v2.286q0 3.589-1.759 6.464t-4.893 4.491-7.063 1.616-7.063-1.616-4.893-4.491-1.759-6.464zM0 10.286v-6.857q0-0.464 0.339-0.804t0.804-0.339h6.857q0.464 0 0.804 0.339t0.339 0.804v6.857q0 0.464-0.339 0.804t-0.804 0.339h-6.857q-0.464 0-0.804-0.339t-0.339-0.804zM18.286 10.286v-6.857q0-0.464 0.339-0.804t0.804-0.339h6.857q0.464 0 0.804 0.339t0.339 0.804v6.857q0 0.464-0.339 0.804t-0.804 0.339h-6.857q-0.464 0-0.804-0.339t-0.339-0.804z" fill="#000000"/>
</svg>
</i><i class="',
        text: 'Мулти лендинг',
        type: 'button',
        tooltip: 'Мулти лендинг',
        format: 'muldilp',
        onclick: onButtonClick
    )