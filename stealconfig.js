System.config({
    map: {
        "*": {
            "can/util/util.js": "can/util/jquery/jquery.js",
            "./jquery/jquery.js": "bower_components/jquery/dist/jquery.min.js"
        }
    },
    paths: {
        "jquery/": "jquerypp/",
        "jquery": "bower_components/jquery/dist/jquery.min.js",
        "jquery/jquery": "bower_components/jquery/dist/jquery.min.js",
        'bower_components/': './bower_components/',
        'bootstrap-dialog': 'bower_components/bootstrap3-dialog/dist/js/bootstrap-dialog.js',
        'bootstrap': 'bower_components/bootstrap/dist/js/bootstrap.min.js',
        //"mootools/mootools.js": "can/lib/mootools-core-1.4.5.js",
        //"dojo/dojo.js": "can/util/dojo/dojo-1.8.1.js",
        //"yui/yui.js": "can/lib/yui-3.7.3.js",
        //"zepto/zepto.js": "can/lib/zepto.1.0rc1.js"
    },

    meta: {
        jquery: {
            format: 'amd',
            exports: "jQuery"
        },

        'jquery/jquery': {
            format: 'amd',
            exports: "jQuery",
            deps: ['jquery']
        },
        'bootstrap-dialog': {
            format: 'global',
            deps: [
                'jquery',
                'bower_components/bootstrap/dist/js/bootstrap.min',
                "bower_components/bootstrap3-dialog/dist/css/bootstrap-dialog.css!"
            ]
        },
        'bower_components/bootstrap/dist/js/bootstrap.min': {
            format: 'global',
            deps: [
                'jquery'
            ]
        },
        'bower_components/redactorjs/redactor': {
            format: 'global',
            exports: "$.fn.redactor",
            deps: [
                'jquery',
                "bower_components/redactorjs/redactor.css!"
            ]
        }

    },

    ext: {
        js: "bower_components/steal/js",
        css: "bower_components/steal/css"
        //less: "steal/less/less.js",
        //coffee: "steal/coffee/coffee.js",
        //ejs: "can/view/ejs/ejs",
        //mustache: "can/view/mustache/mustache"
    }
});
