class MultiLp
  default: {

  }
  constructor: ( type = 'text', name = '' )->
    this.type = type
    this.name = name
    @getUrlParameter()

  urlParameter: null
  getUrlParameter: ()->
    search = window.location.search.substr( 1 )
    keys = {}
    length = 0
    for item in search.split( '&' )
      item = item.split( '=' )
      keys[item[0]] = item[1]
      length++
    if length
      MultiLp::urlParameter = keys


  insertThis: ()->
    unless @urlParameter then return

    id = this.getRandomId()
    document.write( "<span id='#{id}' class='multiLabel' style='display: none !important;'></span>" )
    this.label = label = document.getElementById( id )
    this.parent = parent = label.parentNode
    this.parent.removeChild( this.label )
    paremetrs = @getParameter( parent, this.name )
    @insertValue( parent, paremetrs )

  lastImg: 0
  findImg: ()->
    unless @urlParameter then return
    imgs = document.getElementsByTagName( 'img' )
    for el,index in imgs
      if index < MultiLp::lastImg then return
      paremetrs = @getParameter( el, this.name )
      @insertValue( el, paremetrs )
    MultiLp::lastImg = imgs.length

  lastText: 0
  findText: ()->
    unless @urlParameter then return
    texts = document.getElementsByTagName( 'span' )
    for el,index in texts
      if index < MultiLp::lastText then return
      name = this.getAttr(el, 'data-multi-name')
      unless name then return
      paremetrs = @getParameter( el, this.name )
      @insertValue( el, paremetrs )
    MultiLp::lastText = texts.length

  getParameter: ( element, name )->
    name = this.getAttr( element, 'data-multi-name' ).replace( /\s/gm, '' ).replace( /\n/gm, '' ) unless name
    value = this.getAttr element, 'data-multi-value'
    keys = {}
    if value
      length = 0
      for item in value.split( '&' )
        items = item.split( '=' )
        keys[items[0].replace( /\s/gm, '' ).replace( /\n/gm, '' )] = items[1]
        length++
    {
    name: name || ''
    keys: keys
    }

  insertValue: ( el, {name,keys} )->
    urlValue = @urlParameter[name]
    unless urlValue then return

    valInHtml = keys[urlValue]
    if valInHtml
      urlValue = valInHtml

    urlValue = decodeURIComponent urlValue

    if @type == 'text'
      @insertText( el, urlValue.replace( /_/mg, ' ' ) )
    if @type == 'bg'
      @insertBg( el, urlValue )
    if @type == 'img'
      @insertImg( el, urlValue )
    if @type == 'hide'
      @hideEl( el, urlValue )
    if @type == 'show'
      @showEl( el, urlValue )


  insertText: ( el, text ) ->
    if (!el) then return

    if (document.all)
      if text
        el.innerText = text
    else
      if text
        el.textContent = text

  insertBg: ( el, url )->
    if (!el) then return
    if url
      el.style.backgroundImage = "url(#{url})"

  insertImg: ( el, url )->
    if (!el) then return
    if url
      el.src = url

  hideEl: ( el, url )->
    if (!el) then return
    if url
      el.style.display = 'none'

  showEl: ( el, url )->
    if (!el) then return
    if url
      el.style.display = ''
      el.style.visibility = ''


  #=======================================
  #        Поиск ид
  #=======================================
  ranomIdLitst = []
  getRandomArbitary: ( min = 0, max = 100 ) ->
    Math.floor Math.random() * (max - min) + min
  ranomIdLitst: {}
  getRandomId: ->
    newId = this.getRandomArbitary( 0, 999999 )
    unless MultiLp::ranomIdLitst[newId]
      unless document.getElementById( newId )
        MultiLp::ranomIdLitst[newId] = true
        return newId
      else
        return this.getRandomId()
    else
      return this.getRandomId()


  getAttr: ( ele, attr )->
    result = (ele.getAttribute && ele.getAttribute( attr )) || null
    if( !result )
      attrs = ele.attributes
      length = attrs.length
      for i in [0..length]
        if(attrs[i] and attrs[i].nodeName == attr)
          result = attrs[i].nodeValue
    result

window.multiText = ( name )->
  new MultiLp( 'text', name ).insertThis()
window.multiBg = ( name )->
  new MultiLp( 'bg', name ).insertThis()
window.multiHide = ( name )->
  new MultiLp( 'hide', name ).insertThis()
window.multiShow = ( name )->
  new MultiLp( 'show', name ).insertThis()

window.initMultiImg = ()->
  new MultiLp( 'img' ).findImg()

window.initMultiText = ()->
  new MultiLp( 'text' ).findText()

index = setInterval( ->
#  initMultiImg()
  if "loaded" == document.readyState or document.readyState == "complete"
    clearInterval index
    initMultiImg()
    initMultiText()
, 10 )