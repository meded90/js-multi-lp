// Generated by CoffeeScript 1.8.0
(function() {
  define(['jquery', 'bootstrap-dialog', 'bower_components/bootstrap3-dialog/dist/css/bootstrap-dialog.css!', 'bower_components/font-awesome/css/font-awesome.css!', '../gepLp'], function($, BootstrapDialog) {
    var MultiLpModal;
    return MultiLpModal = (function() {
      MultiLpModal.prototype.getTemplate = function() {
        return "<section> <div class='radio radio-primary'> <label> <input type='radio' name='place'  value='country' " + (this.param.is_country_name ? 'checked' : void 0) + "> Строна  (<i data-geolp='country '>Росия</i>) </label> </div> <div class='radio radio-primary'> <label> <input type='radio' name='place'  value='region'  " + (this.param.is_region_name ? 'checked' : void 0) + "> Регион (<i data-geolp=' region '>Московская</i>) </label> </div> <div class='radio radio-primary'> <label> <input type='radio' name='place'  value='city' " + (this.param.is_city ? 'checked' : void 0) + "> Грород (<i data-geolp='city'>Москва</i>) </label> </div> </section>";
      };

      MultiLpModal.prototype["default"] = {
        vars: '',
        callback: function() {},
        isRemove: false,
        callbackRemove: function() {}
      };

      function MultiLpModal(optiuns) {
        var buttons;
        this.options = $.extend({}, this["default"], optiuns);
        buttons = [];
        buttons.push({
          label: 'Отмена',
          action: (function(_this) {
            return function(dialogRef) {
              return dialogRef.close();
            };
          })(this)
        });
        if (this.options.isRemove) {
          buttons.push({
            cssClass: 'btn-danger',
            label: 'Удалить',
            action: (function(_this) {
              return function(dialogRef) {
                _this.options.callbackRemove();
                return dialogRef.close();
              };
            })(this)
          });
        }
        buttons.push({
          cssClass: 'btn-primary',
          label: 'Вставить',
          action: (function(_this) {
            return function(dialogRef) {
              _this.options.callback(_this.getModalForm(dialogRef.getMessage()));
              return dialogRef.close();
            };
          })(this)
        });
        BootstrapDialog.show({
          title: 'Гео лендинг',
          message: this.render(),
          buttons: buttons,
          onshow: function(model) {
            return model.$modalContent.find('[data-geolp]').each(function() {
              return window.initGeoEl(this);
            });
          }
        });
        return;
      }

      MultiLpModal.prototype.render = function() {
        this.getParameter(this.options.vars);
        this.modal = $(this.getTemplate());
        return this.modal;
      };

      MultiLpModal.prototype.getParameter = function(param) {
        return this.param = {
          is_country_name: param ? /country/im.test(param) : false,
          is_region_name: param ? /region/im.test(param) : false,
          is_city: param ? /city/im.test(param) : true
        };
      };

      MultiLpModal.prototype.getModalForm = function(modal) {
        var name, vars;
        name = encodeURI($.trim(modal.find('[name=name-mark]').val()));
        vars = '';
        modal.find('input').each(function() {
          var $this;
          $this = $(this);
          if ($this.is(':checked')) {
            return vars += $this.val();
          }
        });
        return {
          vars: vars
        };
      };

      return MultiLpModal;

    })();
  });

}).call(this);

//# sourceMappingURL=modal.js.map
