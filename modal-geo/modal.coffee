define(['jquery',
        'bootstrap-dialog',
        'bower_components/bootstrap3-dialog/dist/css/bootstrap-dialog.css!',
        'bower_components/font-awesome/css/font-awesome.css!',
        '../gepLp'
  ],
  ($, BootstrapDialog) ->
    class MultiLpModal
      getTemplate: ->
        return "

<section>
   <div class='radio radio-primary'>
    <label>
    <input type='radio' name='place'  value='country' #{if this.param.is_country_name then 'checked'}> Строна  (<i data-geolp='country '>Росия</i>)
    </label>
  </div>
   <div class='radio radio-primary'>
    <label>
    <input type='radio' name='place'  value='region'  #{if this.param.is_region_name then 'checked'}> Регион (<i data-geolp=' region '>Московская</i>)
    </label>
  </div>
   <div class='radio radio-primary'>
    <label>
    <input type='radio' name='place'  value='city' #{if this.param.is_city then 'checked'}> Грород (<i data-geolp='city'>Москва</i>)
    </label>
  </div>
</section>

"

      default: {
        vars: ''
        callback: ->
        isRemove: false
        callbackRemove: ->
      }
      constructor: (optiuns)->
        this.options = $.extend {}, this.default, optiuns

        buttons= []
        buttons.push({
          label: 'Отмена',
          action: (dialogRef)=>
            dialogRef.close();

        })
        if this.options.isRemove
          buttons.push(
            {
              cssClass: 'btn-danger',
              label: 'Удалить',
              action: (dialogRef)=>
                @options.callbackRemove()
                dialogRef.close()
            })
        buttons.push(
          {
            cssClass: 'btn-primary',
            label: 'Вставить',
            action: (dialogRef)=>
              @options.callback(@getModalForm(dialogRef.getMessage()))
              dialogRef.close();

          })
        BootstrapDialog.show({
          title: 'Гео лендинг',
          message: this.render(),
          buttons: buttons
          onshow: (model)->
            model.$modalContent.find('[data-geolp]').each ->
              window.initGeoEl(this)
        });
        return


      render: ()->
        @getParameter(this.options.vars)
        this.modal = $ this.getTemplate()
        this.modal

      getParameter: (param)->

        @param = {
        is_country_name: if param then /country/im.test param else false
        is_region_name: if param then /region/im.test param else false
        is_city: if param then /city/im.test param else true
        }



      getModalForm: (modal)->
        name = encodeURI $.trim modal.find('[name=name-mark]').val()
        vars = ''
        modal.find('input').each ->
          $this = $ this
          if $this.is(':checked')
            vars += $this.val()
        return {
          vars: vars
        }
)