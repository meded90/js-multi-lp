define( ['jquery',
         'bootstrap-dialog',
         'bower_components/bootstrap3-dialog/dist/css/bootstrap-dialog.css!',
         'bower_components/font-awesome/css/font-awesome.css!'],
  ( $, BootstrapDialog ) ->
    class MultiLpModal
      getTemplate: ->
        return "

<section id='redactor-modal-multilp'>
    <label style='margin-top: 0 !important;'>Параметр URL</label>

    <input type='text' class='col-sx-4 form-control' name='name-mark' value='#{this.options.name}'/>
  <small > <span class='text-muted'>www.example.com</span>?<span class='multilp-name-mark'>#{this.options.name}</span>={Ваше значения}</small>

<h4 style='margin-top: 20px;'>Сохронениы значения</h4>
<div class='multilp-vars'>
</div>
<br/>
<a href='#0' class='btn btn-primary multilp-add-var'><i class='fa fa-plus'></i> Добавить переменную</a>
</section>

"
      getVariablesTemplate: ( name = 'var', value = '' )->
        return "
<div class='redactor-modal-multilp-var' style='margin-bottom: 10px;' >
<div class='row'>
  <div class='col-xs-3'>
      <label>Переменная</label>
      <input type='text'  class='form-control' name='name-var' value='#{name}'/>
  </div>
  <div class='col-xs-8'>
      <label>Значения</label>
      <input type='text' class='form-control' name='value-var' value='#{value}'/>

  </div>
  <div class='col-xs-1'>
      <label>&nbsp;</label>
    <a class='remove-var fa fa-trash btn btn-danger pull-right' style='margin-top: 3px;' href='#0'></a>
  </div>
</div>
<small > <span class='text-muted'>www.example.com</span>?<span class='multilp-name-mark'>#{this.options.name}</span>=<span class='multilp-name-var'>#{name}</span></small>
</div>
"

      default: {
        name: 'utm_term'
        vars: []
        callback: ->
        isRemove: false
        callbackRemove: ->
      }
      constructor: ( optiuns )->
        this.options = $.extend {}, this.default, optiuns
        buttons= []
        buttons.push({
          label: 'Отмена',
          action: ( dialogRef )=>
            dialogRef.close();

        })
        if this.options.isRemove
          buttons.push(
            {
              cssClass: 'btn-danger',
              label: 'Удалить',
              action: ( dialogRef )=>
                @options.callbackRemove()
                dialogRef.close()
            })
        buttons.push(
          {
            cssClass: 'btn-primary',
            label: 'Вставить',
            action: ( dialogRef )=>
              if @isValid()
                @options.callback( @getModalForm( dialogRef.getMessage() ) )
                dialogRef.close();

          })
        BootstrapDialog.show( {
          title: 'Мульти лендинг',
          message: this.render(),
          buttons: buttons
        } );
        return

      isValid:->
        result = true
        helperError =  (el, messeg)->
          el.parent().addClass('has-error')
          el.after("<p class=help-block>#{messeg}</p>") if messeg
        helperSuccesses = (el)->
          el.parent().removeClass('has-error').find('.help-block').remove()

        $input = this.modal.find( 'input' )
        $input.each (index, item)->
          $item = $ item
          unless $.trim $item.val()
            helperError($item, 'Обезательное поля')
            result = false
          else
            helperSuccesses($item)

        result

      render: ()->
        this.modal = $ this.getTemplate()
        @renderVariables( this.modal, @getVars() )
        @afterRender( this.modal )
        this.modal

      getVars: ->
        if typeof this.options.vars == "string"
          for item in this.options.vars.split( '&' )
            item = item.split( '=' )
            unless item[1]
              null
            else
              {
                value: item[1]
                name: item[0]
              }
        else if  $.isArray(this.options.vars)
          this.options.vars

      renderVariables: (modal ,vars)->
        console.log vars
        for item in vars
          modal.find( '.multilp-vars' ).append( this.getVariablesTemplate( item.name, item.value ) ) if item

      afterRender: ( modal ) ->
        modal.on 'keyup', '[name=name-mark]', ( e )=>
          val = encodeURI $.trim $( e.target ).val()
          modal.find( '.multilp-name-mark' ).html( val )
          this.options.name = val

        modal.on 'keyup', '[name=name-var]', ( e )=>
          $this = $( e.target )

          val = encodeURI $.trim $this.val()
          $this.parents( '.redactor-modal-multilp-var' ).find( '.multilp-name-var' ).html( val )

        modal.on 'click', '.multilp-add-var', ( e )=>
          length = modal.find( '.redactor-modal-multilp-var' ).length
          modal.find( '.multilp-vars' ).append( this.getVariablesTemplate( 'val' + length ) )

        modal.on 'click', '.remove-var', ( e )=>
          $this = $( e.target )
          $this.parents( '.redactor-modal-multilp-var' ).remove()

        modal.find( '[name=name-mark]' ).focus()


      getModalForm: ( modal )->
        name = encodeURI $.trim modal.find( '[name=name-mark]' ).val()
        vars = ''
        modal.find( '.redactor-modal-multilp-var' ).each ->
          $this = $ this
          nameVar = encodeURI $.trim $this.find( '[name=name-var]' ).val()
          unless nameVar then return
          val = $this.find( '[name=value-var]' ).val() || ' '
          vars += "&#{nameVar}=#{val}"

        return {
          name: name
          vars: vars[1...]
        }
)